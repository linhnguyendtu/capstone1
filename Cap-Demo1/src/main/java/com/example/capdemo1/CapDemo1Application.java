package com.example.capdemo1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CapDemo1Application {

    public static void main(String[] args) {
        SpringApplication.run(CapDemo1Application.class, args);
    }

}
