package com.example.capdemo1.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewRequest {
    @JsonProperty("title")
    private String title ;

    @JsonProperty("thumbnail")
    private String thumbnail ;

    @JsonProperty("short_description")
    private String shortDescription ;

    @JsonProperty("content")
    private String content;

    @JsonProperty("category_id")
    private String category;

    @JsonProperty("id")
    private long id;

    @JsonProperty("create_by")
    private String createBy;

    @JsonProperty("create_date")
    private Date createDate;

    @JsonProperty("modified_by")
    private String modifiedBy;

    @JsonProperty("modified_date")
    private Date modifiedDate;
}
