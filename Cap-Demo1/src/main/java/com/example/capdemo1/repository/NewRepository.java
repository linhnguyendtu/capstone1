package com.example.capdemo1.repository;

import com.example.capdemo1.model.entity.NewEntity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface NewRepository extends JpaRepository<NewEntity, Long> {
    List<NewEntity> findAllByCategoryId(Long categoryId, Pageable pageable);

    Optional<NewEntity> findFirstById(Long id);
}
