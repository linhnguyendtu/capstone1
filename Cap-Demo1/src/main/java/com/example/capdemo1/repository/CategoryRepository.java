package com.example.capdemo1.repository;

import com.example.capdemo1.model.entity.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository <CategoryEntity, Long> {
    CategoryEntity findFirstByCode(String code);

}
