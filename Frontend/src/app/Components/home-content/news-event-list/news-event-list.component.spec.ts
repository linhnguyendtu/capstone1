import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsEventListComponent } from './news-event-list.component';

describe('NewsEventListComponent', () => {
  let component: NewsEventListComponent;
  let fixture: ComponentFixture<NewsEventListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsEventListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsEventListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
