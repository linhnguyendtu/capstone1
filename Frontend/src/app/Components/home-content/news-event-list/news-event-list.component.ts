import { Component, OnInit } from '@angular/core';
import { News } from 'src/app/Models/news.model';
import { NewsService } from 'src/app/_Services/News/news.service';
import { Router } from '@angular/router';

import { OwlOptions } from 'ngx-owl-carousel-o';
@Component({
  selector: 'app-news-event-list',
  templateUrl: './news-event-list.component.html',
  styleUrls: ['./news-event-list.component.css']
})
export class NewsEventListComponent implements OnInit {
  AllNews:News[];
  constructor(private  newsService: NewsService, private routers:Router) { }

  //Slider tin tức
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    navSpeed: 700,
    navText: ['Pre', 'Next'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: true
  }

  ngOnInit(): void {
  this.getAllNews();
  }
  private getAllNews() {
    this.newsService.getAllNews().subscribe(data=>{
        this.AllNews=data;
        console.log(this.AllNews);
    })
  }
  newDetails(id:number){
    this.routers.navigate(['new-details',id]);
  }
}



