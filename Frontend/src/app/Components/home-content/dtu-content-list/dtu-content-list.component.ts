import { Component, OnInit, Type } from '@angular/core';
import { articles } from 'src/app/_Services/article-service/article.service';

@Component({
  selector: 'app-dtu-content-list',
  templateUrl: './dtu-content-list.component.html',
  styleUrls: ['./dtu-content-list.component.css']
})
export class DtuContentListComponent implements OnInit {
  articles = articles;
  // Mảng output luôn chứa 3 phần tử để hiển thị
  output: Array<any> = new Array();
  private pos: number = 2;
  private length = articles.length;
  constructor() {}

  ngOnInit(): void {
    //Khởi tạo giá trị cho mảng output
    if(this.length > 3) {
      for(var i = 0; i < 3; i++) {  
        this.output[i] = articles[i];
      }
    }
    else {
      for(var i = 0; i < this.length; i++) {  
        this.output[i] = articles[i];
      }
    }
  }
  next() {
    this.output.length = 0;
    if(this.length > 3) {
      if(this.length < (this.pos+4)) {
        this.output = articles.slice(this.pos+1, this.length+1);
      }
      else {
        this.output = articles.slice(this.pos+1, this.pos+4);
      }
    }
  }

  previous() {
    this.output.length = 0;
    if(this.pos > 2) {
      this.output = articles.slice(this.pos-3, this.pos-1);
    } 
    else {
      if(this.length < 3) {
        for(var i = 0; i < this.length; i++) {  
          this.output[i] = articles[i];
        }
      }
      else {
        for(var i = 0; i < 3; i++) {  
          this.output[i] = articles[i];
        }
      }
    }
  }
}
