import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DtuContentDetailComponent } from './dtu-content-detail.component';

describe('DtuContentDetailComponent', () => {
  let component: DtuContentDetailComponent;
  let fixture: ComponentFixture<DtuContentDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DtuContentDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DtuContentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
