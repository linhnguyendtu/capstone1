import { Component, Input, OnInit } from '@angular/core';
import { Article } from 'src/app/_Services/article-service/article.service';
@Component({
  selector: 'app-dtu-content-detail',
  templateUrl: './dtu-content-detail.component.html',
  styleUrls: ['./dtu-content-detail.component.css']
})
export class DtuContentDetailComponent implements OnInit {
  @Input() article: Article;
  constructor() { }

  ngOnInit(): void {
  }
  showDetai() {}
}
