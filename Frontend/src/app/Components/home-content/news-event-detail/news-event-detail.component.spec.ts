import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsEventDetailComponent } from './news-event-detail.component';

describe('NewsEventDetailComponent', () => {
  let component: NewsEventDetailComponent;
  let fixture: ComponentFixture<NewsEventDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsEventDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsEventDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
