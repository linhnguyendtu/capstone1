import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NewsService } from 'src/app/_Services/News/news.service';
import { News } from 'src/app/Models/news.model';



@Component({
  selector: 'app-news-event-detail',
  templateUrl: './news-event-detail.component.html',
  styleUrls: ['./news-event-detail.component.css']
})
export class NewsEventDetailComponent implements OnInit {
  id:number;
  news: News;
  constructor(private router:ActivatedRoute, private newsDetailService: NewsService) { }

  ngOnInit(): void {
    this.id=this.router.snapshot.params['id'];
    this.news=new News();
    this.newsDetailService.getNewsDetailById(this.id).subscribe(data=>{
      this.news=data;
    })
  }
}
