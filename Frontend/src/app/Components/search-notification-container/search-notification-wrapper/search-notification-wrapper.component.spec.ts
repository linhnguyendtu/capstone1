import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchNotificationWrapperComponent } from './search-notification-wrapper.component';

describe('SearchNotificationWrapperComponent', () => {
  let component: SearchNotificationWrapperComponent;
  let fixture: ComponentFixture<SearchNotificationWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchNotificationWrapperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchNotificationWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
