import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutDtuComponent } from './about-dtu.component';

describe('AboutDtuComponent', () => {
  let component: AboutDtuComponent;
  let fixture: ComponentFixture<AboutDtuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AboutDtuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutDtuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
