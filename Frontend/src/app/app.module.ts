import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpHeaders } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CarouselModule } from 'ngx-owl-carousel-o';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';

import { SearchComponent } from './Components/search-notification-container/search/search.component';
import { NotificationComponent } from './Components/search-notification-container/notification/notification.component';

import { HomeComponent } from './Components/header-container/home/home.component';
import { AboutDtuComponent } from './Components/header-container/about-dtu/about-dtu.component';
import { NewsEventComponent } from './Components/header-container/news-event/news-event.component';
import { TuitionComponent } from './Components/header-container/tuition/tuition.component';
import { StudyComponent } from './Components/header-container/study/study.component';
import { ContactComponent } from './Components/header-container/contact/contact.component';
import { AccountManagementComponent } from './Components/header-container/account-management/account-management.component';
import { BannerComponent } from './Components/banner/banner.component';
import { DtuContentListComponent } from './Components/home-content/dtu-content-list/dtu-content-list.component';
import { DtuContentDetailComponent } from './Components/home-content/dtu-content-detail/dtu-content-detail.component';
import { HeaderMenuComponent } from './Components/header-menu/header-menu.component';
import { FooterComponent } from './Components/footer/footer.component';
import { SearchNotificationWrapperComponent } from './Components/search-notification-container/search-notification-wrapper/search-notification-wrapper.component';
import { NewsEventListComponent } from './Components/home-content/news-event-list/news-event-list.component';
import { NewsEventDetailComponent } from './Components/home-content/news-event-detail/news-event-detail.component';
import { NewsService } from './_Services/News/news.service';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    NotificationComponent,
    HomeComponent,
    AboutDtuComponent,
    NewsEventComponent,
    TuitionComponent,
    StudyComponent,
    ContactComponent,
    AccountManagementComponent,
    BannerComponent,
    DtuContentListComponent,
    DtuContentDetailComponent,
    HeaderMenuComponent,
    FooterComponent,
    SearchNotificationWrapperComponent,
    NewsEventListComponent,
    NewsEventDetailComponent 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    CarouselModule 
  ],
  providers: [NewsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
