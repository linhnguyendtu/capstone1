import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AboutDtuComponent } from './Components/header-container/about-dtu/about-dtu.component';
import { AccountManagementComponent } from './Components/header-container/account-management/account-management.component';
import { ContactComponent } from './Components/header-container/contact/contact.component';
import { HomeComponent } from './Components/header-container/home/home.component';
import { NewsEventComponent } from './Components/header-container/news-event/news-event.component';
import { StudyComponent } from './Components/header-container/study/study.component';
import { TuitionComponent } from './Components/header-container/tuition/tuition.component';
import { NewsEventDetailComponent } from './Components/home-content/news-event-detail/news-event-detail.component';




const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: "about",
    component: AboutDtuComponent    
  },
  {
    path: "news-event",
    component: NewsEventComponent
  },
  {
    path: "tuition",
    component: TuitionComponent
  },
  {
    path: "study",
    component: StudyComponent
  },
  {
    path: "contact",
    component: ContactComponent
  },
  {
    path: "account-management",
    component: AccountManagementComponent
  },
  {
    path: "",
    component: HomeComponent
  },
  {path:'home', component: HomeComponent},
  {path:'new-details/:id', component: NewsEventDetailComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
